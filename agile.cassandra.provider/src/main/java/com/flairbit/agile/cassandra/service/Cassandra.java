package com.flairbit.agile.cassandra.service;

import java.time.Instant;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datastax.driver.core.ConsistencyLevel;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.Statement;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.flairbit.agile.cassandra.model.EventMessage;

@Service
public class Cassandra {

	private static final String TABLE_NAME = "events_by_resource";
	private static final String CONTENT_TYPE = "application/json";
	
	private Session session;
	private ObjectMapper mapper;

	@Autowired
	public void setCassandraSession(Session session) {
		this.session = session;
	}
	
	@Autowired
	public void setObjectMapper(ObjectMapper mapper) {
		this.mapper = mapper;
	}
	
	public void saveJson (Instant timestamp, String resourceId, String capabilityId, Object message) {
		EventMessage json;
		try {
			json = EventMessage
					.builder(
							UUID.randomUUID().toString(),
							resourceId, 
							capabilityId, 
							timestamp, 
							mapper.writeValueAsString(message))
					.withContentType(CONTENT_TYPE)
					.withSerManifest(mapper.getClass().getName() + "/" + mapper.version())
					.withSerName(mapper.getClass().getName()).build();
		} catch (JsonProcessingException e) {
			throw new RuntimeException(e);
		}
		Statement insert = QueryBuilder
				.insertInto(TABLE_NAME)
				.json(json)
				.setConsistencyLevel(ConsistencyLevel.LOCAL_QUORUM);
		session.execute(insert);
	}
}
