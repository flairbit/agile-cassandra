package com.flairbit.agile.cassandra.codecs;

import com.datastax.driver.core.DataType;
import com.datastax.driver.core.ParseUtils;
import com.datastax.driver.core.ProtocolVersion;
import com.datastax.driver.core.TypeCodec;
import com.datastax.driver.core.exceptions.InvalidTypeException;
import com.datastax.driver.core.utils.Bytes;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.google.common.reflect.TypeToken;
import java.io.IOException;
import java.nio.ByteBuffer;

public class JacksonJsonCodec<T> extends TypeCodec<T> {

	private final ObjectMapper objectMapper;

	public JacksonJsonCodec(Class<T> javaClass) {
		this(javaClass, new ObjectMapper());
	}

	public JacksonJsonCodec(TypeToken<T> javaType) {
		this(javaType, new ObjectMapper());
	}

	public JacksonJsonCodec(Class<T> javaClass, ObjectMapper objectMapper) {
		this(TypeToken.of(javaClass), objectMapper);
	}

	public JacksonJsonCodec(TypeToken<T> javaType, ObjectMapper objectMapper) {
		super(DataType.varchar(), javaType);
		this.objectMapper = objectMapper;
	}

	@Override
	public ByteBuffer serialize(T value, ProtocolVersion protocolVersion)
			throws InvalidTypeException {
		if (value == null) return null;
		try {
			return ByteBuffer.wrap(objectMapper.writeValueAsBytes(value));
		} catch (JsonProcessingException e) {
			throw new InvalidTypeException(e.getMessage(), e);
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public T deserialize(ByteBuffer bytes, ProtocolVersion protocolVersion)
			throws InvalidTypeException {
		if (bytes == null) return null;
		try {
			return (T) objectMapper.readValue(Bytes.getArray(bytes), toJacksonJavaType());
		} catch (IOException e) {
			throw new InvalidTypeException(e.getMessage(), e);
		}
	}

	@Override
	public String format(T value) throws InvalidTypeException {
		if (value == null) return "NULL";
		String json;
		try {
			json = objectMapper.writeValueAsString(value);
		} catch (IOException e) {
			throw new InvalidTypeException(e.getMessage(), e);
		}
		return ParseUtils.quote(json);
	}

	@Override
	@SuppressWarnings("unchecked")
	public T parse(String value) throws InvalidTypeException {
		if (value == null || value.isEmpty() || value.equalsIgnoreCase("NULL")) return null;
		if (!ParseUtils.isQuoted(value))
			throw new InvalidTypeException("JSON strings must be enclosed by single quotes");
		String json = ParseUtils.unquote(value);
		try {
			return (T) objectMapper.readValue(json, toJacksonJavaType());
		} catch (IOException e) {
			throw new InvalidTypeException(e.getMessage(), e);
		}
	}

	protected JavaType toJacksonJavaType() {
		return TypeFactory.defaultInstance().constructType(getJavaType().getType());
	}
}