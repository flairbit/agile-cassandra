package com.flairbit.agile.cassandra;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AgileCassandraApplication {
	public static void main(String[] args) {
		SpringApplication.run(AgileCassandraApplication.class, args);
	}
}
