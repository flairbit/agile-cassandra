package com.flairbit.agile.cassandra.config;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.osgi.util.converter.Converters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Cluster.Builder;
import com.datastax.driver.core.CodecRegistry;
import com.datastax.driver.core.QueryOptions;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.policies.DCAwareRoundRobinPolicy;
import com.datastax.driver.core.policies.ExponentialReconnectionPolicy;
import com.datastax.driver.core.policies.TokenAwarePolicy;
import com.datastax.driver.extras.codecs.jdk8.InstantCodec;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.flairbit.agile.cassandra.codecs.JacksonJsonCodec;
import com.flairbit.agile.cassandra.model.EventMessage;
import com.flairbit.agile.cassandra.model.EventMessageMixin;

@ConfigurationProperties("timeseries")
class CassandraProperties {
	
	private Map<String,String> cassandra = new HashMap<>();
	
	public Map<String, String> getCassandra() {
		return cassandra;
	}
	
	public void setCassandra(Map<String, String> cassandra) {
		this.cassandra = cassandra;
	}
}

@Configuration
@EnableConfigurationProperties(CassandraProperties.class)
public class SessionConfiguration {
	
	@Bean
	public Session session(
			@Autowired JacksonConfiguration jackson,
			@Autowired CassandraProperties configMap) {
		SessionProperties config = Converters.standardConverter()
				.convert(configMap.getCassandra()).to(SessionProperties.class);
		
		ObjectMapper cassandraMapper = jackson.mapper();
		cassandraMapper.addMixIn(EventMessage.class, EventMessageMixin.class);
		
		Builder builder = builder(config, cassandraMapper);
		Session session = builder.build().connect(config.keyspace());
		
		return session;
	}
	
	private Builder builder(SessionProperties config, ObjectMapper mapper) {
		final int defaultPort = config.port();
		List<InetAddress> hosts = getContactPoints(config.contactPoints(), defaultPort);

		long reconnectDelay = config.maxReconnectDelayMs();
		int maxResultSize = config.maxResultSize();
		Builder builder = Cluster.builder().addContactPoints(hosts)
				.withReconnectionPolicy(
						new ExponentialReconnectionPolicy(1000L, reconnectDelay))
				.withQueryOptions(new QueryOptions().setFetchSize(maxResultSize));

		builder = builder.withCredentials(
				config.username(),
				config.password());

		CodecRegistry codecRegistry = CodecRegistry.DEFAULT_INSTANCE;
		codecRegistry.register(InstantCodec.instance);
		codecRegistry.register(new JacksonJsonCodec<EventMessage>(EventMessage.class, mapper));
		
		builder.withCodecRegistry(codecRegistry);
		
		if (!config.localDatacenter().isEmpty()) {
			builder = builder.withLoadBalancingPolicy(
					new TokenAwarePolicy(
							DCAwareRoundRobinPolicy.builder()
							.withLocalDc(config.localDatacenter())
							.withUsedHostsPerRemoteDc(config.usedHostsPerRemoteDC())
							.build()));
		}

		return builder;
	}

	private List<InetAddress> getContactPoints(String contactPoints, final int port) {
		List<String> addresses = Arrays.asList(contactPoints.split(","));
		List<InetAddress> hosts = addresses.stream().map(adr -> adr.split(":")).map(adrarr -> {
			if (adrarr.length == 2) {
				return new InetSocketAddress(adrarr[0].trim(), Integer.parseInt(adrarr[1].trim()));
			} else {
				return new InetSocketAddress(adrarr[0].trim(), port);
			}
		}).map(InetSocketAddress::getAddress).collect(Collectors.toList());
		return hosts;
	}
}
