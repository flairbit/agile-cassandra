package com.flairbit.agile.cassandra.config;

public @interface SessionProperties {
	int port() default 9042;
	String contactPoints() default "localhost";
	String keyspace() default "";
	String username() default "cassandra";
	String password() default "cassandra";

	String localDatacenter() default "";
	int usedHostsPerRemoteDC() default 0;

	int maxResultSize() default 1000;

	int maxReconnectDelayMs() default 15000;

}
