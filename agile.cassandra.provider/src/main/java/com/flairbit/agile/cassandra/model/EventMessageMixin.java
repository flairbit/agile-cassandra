package com.flairbit.agile.cassandra.model;

import java.time.Instant;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EventMessageMixin extends EventMessage {

	@JsonProperty("event_id")
	@Override
	public String getEventId() {
		return super.getEventId();
	}

	@JsonProperty("resource_id")
	@Override
	public String getResourceId() {
		return super.getResourceId();
	}

	@JsonProperty("capability_id")
	@Override
	public String getCapabilityId() {
		return super.getCapabilityId();
	}

	@JsonProperty("timestamp")
	@Override
	public Instant getTimestamp() {
		return super.getTimestamp();
	}

	@Override
	public String getPayload() {
		return super.getPayload();
	}

	@JsonProperty("tbucket")
	@Override
	public int getTimeBucket() {
		return super.getTimeBucket();
	}

	@JsonProperty("content_type")
	@Override
	public String getContentType() {
		return super.getContentType();
	}

	@JsonProperty("ser_name")
	@Override
	public String getSerName() {
		return super.getSerName();
	}

	@JsonProperty("ser_manifest")
	@Override
	public String getSerManifest() {
		return super.getSerManifest();
	}
}
