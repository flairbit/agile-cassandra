package com.flairbit.agile.cassandra.model;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class EventMessage {

	private final static ZoneId UTC_ZONE = ZoneId.of("UTC");
	private final static DateTimeFormatter timeBucketFormatter = DateTimeFormatter.ofPattern("yyyyMM").withZone(UTC_ZONE);
	
	private String eventId;
	private String resourceId;
	private String capabilityId;
	private Instant timestamp;
	private int timeBucket;
	
	private String contentType;
	private String serName;
	private String serManifest;
		
	private String payload;

	protected EventMessage() {}
	
	protected EventMessage(String eventId, String resourceId, String capabilityId, Instant timestamp, String payload) {
		
		this.eventId = eventId;
		this.resourceId = resourceId;
		this.capabilityId = capabilityId;
		this.timestamp = timestamp;
		this.payload = payload;
	}
	
	public static Builder builder (String eventId, String resourceId, String capabilityId, Instant timestamp, String payload) {
		return new Builder(eventId, resourceId, capabilityId, timestamp, payload);
	}
	
	public static class Builder {
		
		private final String eventId;
		private final String resourceId;
		private final String capabilityId;
		private final Instant timestamp;
		private final int timeBucket;
		
		private final String payload;
		
		private String contentType;
		private String serName;
		private String serManifest;
		
		public Builder(String eventId, String resourceId, String capabilityId, Instant timestamp, String payload) {
			this.eventId = eventId;
			this.resourceId = resourceId;
			this.capabilityId = capabilityId;
			this.timestamp = timestamp;
			
			this.payload = payload;
			
			this.timeBucket = bucketOf(timestamp);
		}

		public Builder withContentType(String contentType) {
			this.contentType = contentType;
			return this;
		}
		public Builder withSerName(String serName) {
			this.serName = serName;
			return this;
		}
		public Builder withSerManifest(String serManifest) {
			this.serManifest = serManifest;
			return this;
		}
		
		public EventMessage build() {
			EventMessage evt = new EventMessage(eventId, resourceId, capabilityId, timestamp, payload);
			evt.contentType = this.contentType;
			evt.serManifest = this.serManifest;
			evt.serName = this.serName;
			evt.timeBucket = this.timeBucket;
			return evt;
		}
	}

	public String getEventId() {
		return eventId;
	}

	public void setEventId(String eventId) {
		this.eventId = eventId;
	}

	public String getResourceId() {
		return resourceId;
	}

	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}

	public String getCapabilityId() {
		return capabilityId;
	}

	public void setCapabilityId(String capabilityId) {
		this.capabilityId = capabilityId;
	}

	public Instant getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Instant timestamp) {
		this.timestamp = timestamp;
	}

	public String getPayload() {
		return payload;
	}

	public void setPayload(String payload) {
		this.payload = payload;
	}
	
	public int getTimeBucket() {
		return timeBucket;
	}

	public void setTimeBucket(int timeBucket) {
		this.timeBucket = timeBucket;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getSerName() {
		return serName;
	}

	public void setSerName(String serName) {
		this.serName = serName;
	}

	public String getSerManifest() {
		return serManifest;
	}

	public void setSerManifest(String serManifest) {
		this.serManifest = serManifest;
	}

	private static int bucketOf(Instant timestamp) {
		return Integer.parseInt(timeBucketFormatter.format(timestamp));
	}
}
