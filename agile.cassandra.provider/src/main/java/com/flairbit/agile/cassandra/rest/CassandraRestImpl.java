package com.flairbit.agile.cassandra.rest;

import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.flairbit.agile.cassandra.service.Cassandra;

@CrossOrigin
@RestController
@RequestMapping(value = "/cassandra",
	produces = MediaType.APPLICATION_JSON_VALUE)
public class CassandraRestImpl {

	public static class EventREST {
		public Instant timestamp;
		public String resource;
		public String capability;
		public Object message;
	}
	
	private static final Logger logger = LoggerFactory.getLogger(CassandraRestImpl.class);

	@Autowired
	private Cassandra cassandra;

	public void activate() {
		logger.info("Cassandra REST endpoint is now active");
	}
	public void deactivate() {
		logger.info("Cassandra REST endpoint: deactivated");
	}
	
	@RequestMapping(
			value = "/", 
			consumes="application/json",
			method = RequestMethod.POST)
	public void save(@RequestBody EventREST payload) {
		if (payload != null)
			cassandra.saveJson(
				Optional.ofNullable(payload.timestamp).orElse(Instant.now()),
				payload.resource, 
				payload.capability, 
				payload.message);
	}

}
