package agile.cassandra.provider;

import java.time.Instant;
import java.util.UUID;

import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.flairbit.agile.cassandra.config.JacksonConfiguration;
import com.flairbit.agile.cassandra.model.EventMessage;

public class SerializationTest {

	private static final String resourceId = "resource:1";
	private static final String capabilityId = "capability:1";
	private static final String CONTENT_TYPE = "application/json";

	@Test
	public void testSerialization() throws Exception {
		JacksonConfiguration jackson = new JacksonConfiguration();
		ObjectMapper mapper = jackson.mapper();
		
		EventMessage json = EventMessage
				.builder(
						UUID.randomUUID().toString(),
						resourceId, 
						capabilityId, 
						Instant.now(), 
						"message")
				.withContentType(CONTENT_TYPE)
				.withSerManifest("agile.cassandra.provider")
				.withSerName("jackson").build();
		System.out.println(mapper.writeValueAsString(json));
		
	}
}
