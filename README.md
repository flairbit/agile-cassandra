# Introduction
This guide illustrates the steps to enable Big-Data Analytics on-top-of Agile IoT stack using the source code provided [here](https://bitbucket.org/flairbit/agile-cassandra/src/develop/ "Apache Cassandra connector for Agile IoT stack").
In particular, this public repository contains the source code of a software component named **agile-iot-cassandra-srv** developed to store data coming from Agile IoT gateway into Apache Cassandra.
This software component has been developed with Spring Boot and it can be embedded into a docker image. Moreover, a Node-RED workflow is also available to automate the process of transferring data from the Agile IoT gateway into the Apache Cassandra storage. It allows to collect data coming from a sensor connected to the Agile IoT gateway. These data are then aggregated and stored into the NoSQL database by using a RESTful endpoint provided by the agile-iot-cassandra-srv component. Finally, in order to enable data analytics on-top-of the gathered data, Apache Zeppelin can be connected to Apache Cassandra and can be used to design and run advanced data preparation and processing workflows to extract insights from available data.

## The sample application
A (virtual) sensor sends data to the Agile IoT gateway. By using the Apache Cassandra connector µ-service these data are stored into Apache Cassandra database. Finally, it is possible to use Apache Zeppelin to access the data stored into Apache Cassandra database and to design analytics workflows on-top-of the collected data.

## Scknowledgements and Background
[FlairBit](http://www.flairbit.com "FlairBit's Homepage") is an Italian SME founded in 2015 in Genoa, a software development company and an IoT/Big Data platform ([SenseIoTy](http://www.senseioty.com "Senseioty's Homepage")), solution, and service provider. It provides consultancy services and scalable, reliable, secure, and complete software productss for the IoT and Big Data markets. Senseioty is composed by open source frameworks and technologies on top of which specific IoT and Big-Data analytics projects and solutions at scale can be efficiently designed, developed and delivered. FlairBit is actively working on different scenarios: Fleet Management (e.g., connected vehicle), Smart Agriculture (e.g., connected tractors, precision agriculture), Healthcare, Energy Management and efficiency, Smart City, Connected products (e.g., coffee machines, vending machines), etc.

[FlairBit is one of the eight companies selected during the 2nd Open Call of Agile IoT](https://agile-iot.eu/2018/06/15/announcing-the-8-winners-of-the-agile-iot-open-call-2/ "Agile IoT OC winners"). The main objective of the experiment regards the integration of Senseioty, the FlairBit software platform to accelerate the Industrial Internet of Things (IoT) Big Data Analytics, with the Agile IoT gateway and services. This integration allowed Senseioty to benefit in flexibility both in terms of data ingestion and edge computing capabilities. Moreover, the Agile Gateway modularity at the hardware level provided support for various wireless and wired IoT networking technologies and Senseioty benefits from these additional protocols as a way to create new data sources and IoT connectors. On the other hand, Senseioty offers a prototype of an usable Distributed Data Stream Processor (DDSP), leveraging artificial intelligence to anticipate problems, detect unexpected event patterns and to optimize processes, services and decisions. This component has been integrated with the Agile IoT gateway enabling new innovative services at edge level and making such services available in the Agile IoT market place and ecosystem through Senseioty.

Although the Senseioty platform is proprietary, in [this repository](https://bitbucket.org/flairbit/agile-cassandra/src/develop/ "Apache Cassandra connector for Agile IoT stack") some software modules in order to enable Big-Data Analytics on-top-of Agile IoT stack are provided.

This guide illustrates the steps to enable Big-Data Analytics on-top-of Agile IoT stack using the source code provided [here](https://bitbucket.org/flairbit/agile-cassandra/src/develop/ "Apache Cassandra connector for Agile IoT stack").

# Requirements
Please find below what it is needed to run this sample:

* Basic knowledge of the Agile IoT stack and the tools needed to handle the Agile IoT stack
* Basic knowledge of Docker
* An [Apache Cassandra](http://cassandra.apache.org/ "Apache Cassandra") database up & running. A docker image is available [here](https://hub.docker.com/_/cassandra/ "Apache Cassandra Docker Image").
* An Agile IoT stack up & running. It may run on a [Raspberry PI 3 model b+](https://www.raspberrypi.org/products/raspberry-pi-3-model-b-plus/) or on a laptop, PC, etc.
* Optionally a [Texas Instruments CC2650STK sensor](http://www.ti.com/tool/CC2650STK-RD "CC2650STK sensor") connected to the Agile IoT gateway to generate data to be stored on Apache Cassandra. If you do not have the sensor, you can use the _dummy_ device feature of the Agile IoT stack in order to generate random data.
* Optionally an [Apache Zeppelin](https://zeppelin.apache.org/ "Apache Zeppelin") web server up & running to design analytics workflows on top of data stored into Apache Cassandra. A docker image is available [here](https://hub.docker.com/r/apache/zeppelin/ "Apache Zeppelin Docker Image").

# Run Apache Cassandra by using docker
Start an Apache Cassandra instance is easy

```bash
$ docker run --name agile-cassandra-database -d cassandra:tag
```

where `agile-cassandra-database` is the name you want to assign to your container and `tag` is the version (e.g., 3.10) of Apache Cassandra.

For further details, please refer to [official documentation here](https://docs.docker.com/samples/library/cassandra/).

# Apache Cassandra connector for Agile IoT stack
It is a µ-service used to route data coming from Agile IoT gateway towards an Apache Cassandra NoSQL Big-Data storage. On the one hand, it provides a set of RESTful web services to allow third parties (e.g., the Agile IoT gateway) to store data into Apache Cassandra.

It is possible to build a docker image to run the µ-service and extend the Agile IoT stack via _docker compose_.

## Build the docker image
```bash
mvn package docker:build \
	-Ddocker-image-name=agile-iot-cassandra-srv \
	-DskipTests \
	-pl :agile.cassandra.provider
```

## How to use it from the Agile IoT gateway

After you have created the keyspace and column family in Apache Cassandra using the scripts in `/src/main/resources/cql/ddl.cql`.

Launch the application and save events with:

```bash
curl -X POST  -H 'Content-Type: application/json' \
	http://localhost:8080/cassandra/ 
	-d '{
		"timestamp" : "2018-01-01T00:00:00Z", 
		"resource" : "resource:1", 
		"capability":"capability:2", 
		"message" : {"value":"simple message"}
	}'
```

The `message` field can be anything:

```bash
curl -X POST  -H 'Content-Type: application/json' \
	http://localhost:8080/cassandra/ 
	-d '{
		"timestamp" : "2018-01-01T00:00:00Z", 
		"resource" : "resource:1", 
		"capability":"capability:2", 
		"message" : 42.0
	}'
```

## Configuration
In `/src/main/resources/application.properties` are located the configuration properties of `agile.cassandra.provider` µ-service.

Using this file it is possible to set where the Apache Cassandra database is located and where the RESTful endpoints for integration with 3rd party systems are published.

Please find below the default values.
```
logging.level.root=INFO

server.contextPath=${CONTEXT_PATH:}
server.port=${SERVER_PORT:8080}

timeseries.cassandra.port=${PORT:9042}timeseries.cassandra.contactPoints=${CONTACT_POINTS:localhost}
timeseries.cassandra.keyspace=${KEYSPACE:agile_iot_timeseries}
timeseries.cassandra.username=${USERNAME:cassandra}
timeseries.cassandra.password=${PASSWORD:cassandra}
timeseries.cassandra.localDatacenter=${LOCAL_DATA_CENTER:}
timeseries.cassandra.usedHostsPerRemoteDC=${USED_HOSTS_PER_REMOTE_DC:0}
timeseries.cassandra.maxResultSize=${MAX_RESULT_SIZE:1000}
timeseries.cassandra.maxReconnectDelayMs=${MAX_RECONNECT_DELAY_MS:15000}
```

## Extend the Agile IoT stack to execute the Apache Cassandra connector for the Agile IoT stack
Once you have built the Apache Cassandra connector µ-service, it is possible to extend the Agile IoT `docker-compose.yml` file to add to the stack this component.

Please note that, the docker image contains the `applicaiton.properties` file which has to be modified for connecting to the Apache Cassandra database.

```bash
#-------------------------------------------------------------------------------
# Copyright (C) 2017 Resin.io, UNI Passau, FBK.
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     Resin.io, UNI Passau, FBK - initial API and implementation
#-------------------------------------------------------------------------------
version: '2'

services:
#  agile-example:
#    container_name: agile-example
#    build: apps/agile-example
#    # image: agileiot/agile-example-$AGILE_ARCH:v0.1.1
#    depends_on:
#      - agile-core
#    command: node index.js

[...omissis...]

  agile-cassandra:
    container_name: agile-cassandra
    image: agile-iot-cassandra-srv:latest
    restart: always
    ports:
      - 4040:8080
    build:
      context: ./../agile-cassandra/agile.cassandra.provider/target/docker/agile-iot-cassandra-srv/build
      dockerfile: Dockerfile
```

## Additional Resources
In the directory *node_red_flows* are located few NodeRed flows designed with Agile IOT stack in mind.

In particular, `join.ws.json` collects multiple streams (in the example we are using DummyDate component). The flow consist in different WS node joined and aggregated. The resulting aggregation is wrapped and persisted then on Cassandra.

In the directory */src/main/docker* is located a `docker-compose.yml`. The resulting environment of this multi-container application is an extension of the default `agile-stack`, we're infact including `agile.cassandra.provider`. The docker-compose.yml was originally obtain from `agile-stack v0.4.2`.

# Run Apache Zeppelin by using docker
Start an Apache Zeppelin instance is easy

```bash
$ docker run -p 7077:7077 -p 8080:8080 --privileged=true -v $PWD/logs:/logs -v $PWD/notebook:/notebook \
-e ZEPPELIN_NOTEBOOK_DIR='/notebook' \
-e ZEPPELIN_LOG_DIR='/logs' \
-d ${DOCKER_USERNAME}/zeppelin-release:<release-version> \
/usr/local/zeppelin/bin/zeppelin.sh
```

where `<release-version>` is the version (e.g., 0.8.0) of Apache Zeppelin.

For further details, please refer to [official documentation here](https://zeppelin.apache.org/docs/0.7.0/install/docker.html).

## Configure Apache Zeppelin to use Apache Cassandra

For details on how to use [Cassandra Query Language (CQL)](https://docs.datastax.com/en/cql/3.3/cql/cqlIntro.html) in Apache Zeppelin to query the Apache Cassandra database, please refer to [this guide](https://zeppelin.apache.org/docs/0.8.0/interpreter/cassandra.html).

# License
This repository, the contained source code and the accompanying materials are made available under the terms of the Eclipse Public License 2.0 which accompanies this distribution, and is available at https://www.eclipse.org/legal/epl-2.0/